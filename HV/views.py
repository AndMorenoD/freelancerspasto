from django.shortcuts import render
from .models import FreeLancer
from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView, CreateView, DeleteView



def listaHV(request):
    return render(request, 'listaLancers.html')

def lancers_base(request):
    lancers = FreeLancer.objects.all()
    context = {'lista_lancers': lancers}
    return render(request, 'listaLancers.html', context)

class LancerCreate(CreateView):
    model = FreeLancer
    fields = '__all__'
    template_name = 'lancerCreate.html'
    success_url = reverse_lazy('lista_lancers')